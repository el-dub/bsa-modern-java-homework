package com.binary_studio.academy_coin;

import java.util.stream.Stream;

public final class AcademyCoin {

    private AcademyCoin() {
    }

    static class AcademyCoinAccumulator {

        public final int profit;
        public final Integer val;

        private AcademyCoinAccumulator(int profit, Integer val) {
            this.profit = profit;
            this.val = val;
        }

        public static AcademyCoinAccumulator of(int price) {
            return new AcademyCoinAccumulator(0, price);
        }

        public static AcademyCoinAccumulator empty() {
            return new AcademyCoinAccumulator(0, Integer.MAX_VALUE);
        }

        public static AcademyCoinAccumulator concat(AcademyCoinAccumulator a, AcademyCoinAccumulator b) {
            return new AcademyCoinAccumulator(a.profit + Math.max(b.val - a.val, 0), b.val);
        }
    }

    public static int maxProfit(Stream<Integer> prices) {
        return prices
                .map(AcademyCoinAccumulator::of)
                .reduce(AcademyCoinAccumulator.empty(), AcademyCoinAccumulator::concat)
                .profit;
    }

}
