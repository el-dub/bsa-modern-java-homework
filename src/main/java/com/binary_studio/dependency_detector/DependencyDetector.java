package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class DependencyDetector {

    private static final int NOT_VISITED = 0;
    private static final int VISITED = 1;
    private static final int RESOLVED = 2;

    private DependencyDetector() {
    }

    public static boolean canBuild(DependencyList libraries) {
        Map<String, List<String>> dependencies = new HashMap<>();
        for (var dependency : libraries.dependencies) {
            if (!dependencies.containsKey(dependency[0])) {
                dependencies.put(dependency[0], new ArrayList<>());
            }
            dependencies.get(dependency[0]).add(dependency[1]);
        }

        var resolutionStatuses = new HashMap<String, Integer>();

        for (String library : libraries.libraries) {
            if (!checkCanResolve(dependencies, library, resolutionStatuses)) {
                return false;
            }
        }

        return true;
    }

    private static boolean checkCanResolve(Map<String, List<String>> dependencies, String lib, Map<String,
            Integer> resolutionStatuses) {
        int status = resolutionStatuses.getOrDefault(lib, NOT_VISITED);

        if (status == VISITED) {
            return false;
        }

        if (status == RESOLVED) {
            return true;
        }

        resolutionStatuses.put(lib, VISITED);

        for (String dependency : dependencies.getOrDefault(lib, List.of())) {
            if (!checkCanResolve(dependencies, dependency, resolutionStatuses)) {
                return false;
            }
        }

        resolutionStatuses.put(lib, RESOLVED);
        return true;
    }

}
