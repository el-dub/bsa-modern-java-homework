package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final String name;

	private final PositiveInteger maxShieldHP;

	private PositiveInteger currentShieldHP;

	private final PositiveInteger maxHullHP;

	private PositiveInteger currentHullHP;

	private final PositiveInteger maxCapacitorAmount;

	private PositiveInteger currentCapacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(DockedShip dockedShip) {
		this.name = dockedShip.getName();
		this.maxShieldHP = dockedShip.getShieldHP();
		this.currentShieldHP = new PositiveInteger(this.maxShieldHP.value());
		this.maxHullHP = dockedShip.getHullHP();
		this.currentHullHP = new PositiveInteger(this.maxHullHP.value());
		this.maxCapacitorAmount = dockedShip.getCapacitorAmount();
		this.currentCapacitorAmount = new PositiveInteger(this.maxCapacitorAmount.value());
		this.capacitorRechargeRate = dockedShip.getCapacitorRechargeRate();
		this.speed = dockedShip.getSpeed();
		this.size = dockedShip.getSize();
		this.attackSubsystem = dockedShip.getAttackSubsystem();
		this.defenciveSubsystem = dockedShip.getDefenciveSubsystem();
	}

	@Override
	public void endTurn() {
		if (this.currentCapacitorAmount.value() + this.capacitorRechargeRate.value() <= this.maxCapacitorAmount
				.value()) {
			this.currentCapacitorAmount = new PositiveInteger(
					this.currentCapacitorAmount.value() + this.capacitorRechargeRate.value());
		}
		else {
			this.currentCapacitorAmount = new PositiveInteger(this.maxCapacitorAmount.value());
		}
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.attackSubsystem.getCapacitorConsumption().value() > this.currentCapacitorAmount.value()) {
			return Optional.empty();
		}
		else {
			this.currentCapacitorAmount = new PositiveInteger(
					this.currentCapacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value());
			return Optional
					.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
		}
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		attack = this.defenciveSubsystem.reduceDamage(attack);
		if (this.currentShieldHP.value() >= attack.damage.value()) {
			this.currentShieldHP = new PositiveInteger(this.currentShieldHP.value() - attack.damage.value());
		}
		else {
			if (this.currentHullHP.value() - (attack.damage.value() - this.currentShieldHP.value()) <= 0) {
				return new AttackResult.Destroyed();
			}
			this.currentHullHP = new PositiveInteger(
					this.currentHullHP.value() - (attack.damage.value() - this.currentShieldHP.value()));
			this.currentShieldHP = new PositiveInteger(0);
		}
		return new AttackResult.DamageRecived(attack.weapon, attack.damage, attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.defenciveSubsystem.getCapacitorConsumption().value() > this.currentCapacitorAmount.value()) {
			return Optional.empty();
		}
		else {
			this.currentCapacitorAmount = new PositiveInteger(
					this.currentCapacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
			RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();
			PositiveInteger regenerateShield = new PositiveInteger(regenerateAction.shieldHPRegenerated.value());
			PositiveInteger regenerateHull = new PositiveInteger(regenerateAction.hullHPRegenerated.value());
			if (this.maxShieldHP.value() - this.currentShieldHP.value() < regenerateShield.value()) {
				regenerateShield = new PositiveInteger(this.maxShieldHP.value() - this.currentShieldHP.value());
			}
			if (this.maxHullHP.value() - this.currentHullHP.value() < regenerateHull.value()) {
				regenerateHull = new PositiveInteger(this.maxHullHP.value() - this.currentHullHP.value());
			}
			return Optional.of(new RegenerateAction(regenerateShield, regenerateHull));
		}
	}

}
